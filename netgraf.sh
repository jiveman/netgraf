#!/bin/bash
PATH=/home/jaime/env/netgraf/bin:/usr/local/bin:/usr/bin:/bin:/usr/games:/home/jaime/.local/bin:/home/jaime/.local/lib/python3.7/site-packages

source /home/jaime/env/netgraf/bin/activate && cd /home/jaime/Projects/netgraf && ./parse_log.py /var/log/threatstop/threatstop.log
