# NetGraf

Is an iptables / ipset log parser that stores blocks in influxdb for visualization with Grafana.

## usage

    ./parse_log.py <filename>

## installation
```
mkvirtualenv netgraf -p python3
pip install -r requirements.txt
workon netgraf
./parse_log.py /var/log/iptables_blocks.log
```
