#!/usr/bin/env python
import re
import fileinput
from collections import Counter
import datetime
from pathlib import Path
import ipdb
import json
import requests
import logging
import sys
import subprocess
import ipaddress

from influxdb import InfluxDBClient
from diskcache import Cache



# logging.basicConfig(filename="parser.log", level=logging.DEBUG)

# logger = logging.getLogger('simple_example')

from loguru import logger

logger.remove()
logger.add(sys.stderr, colorize=True, format="[<cyan>{time:MM-DD HH:mm:ss}</cyan>] -  <level>{message}</level>", level="DEBUG")
logger.add("app.log", backtrace=True, format="{time:MM-DD HH:mm:ss}[{level}] {message}", rotation="5 MB")

client = InfluxDBClient(host='localhost', port=8086)

cache = Cache(disk_min_file_size=10, directory='_cache')

pair_re = re.compile('([^ ]+)=([^ ]+)')

database_name = 'netgraf'
ip_network = ipaddress.ip_network('192.168.1.0/24')


class LogParser:

    def get_hostname_for_ip(self, ip):
        try:
            hostname = cache.get(ip)
            if not hostname:
                output = subprocess.run(["host", ip, '192.168.1.1', '|', 'tail', '-1', '|', 'awk', '"{print $5}"'], capture_output=True)
                logger.debug('querying for {} returned {}'.format(ip, hostname))
                if output:
                    hostname = output.stdout.split()[-1].decode('utf-8').replace('.local.', '')
                    logger.debug("Caching {} => {}".format(ip, hostname))
                    cache.set(ip, hostname, expire=86400 * 14)
            return hostname
        except Exception as err:
            logger.error("Failed finding hostname for IP {} Err: {}".format(ip, err))

    def get_or_create_db(self):
        dbs = client.get_list_database()
        names = [ x['name'] for x in dbs ]
        if self.database in names:
            client.switch_database(self.database)
        else:
            print('creating {} db...'.format(self.database))
            client.create_database(self.database)
            client.switch_database(self.database)


    def __init__(self):
        self.resumed = False
        self.lifetime = {}
        self.database = database_name
        self.config = "./netgraf_data.json"
        self.__tstamp = 0
        self.blocks = []
        self.UTC_OFFSET_TIMEDELTA = datetime.datetime.utcnow() - datetime.datetime.now()

        self.wan_ips = set()
        self.wan_ips.add(requests.get('https://api.ipify.org').text)

        self.get_or_create_db()

        if not self.tstamp:
            data = {}
            if cache.get('run_state'):
                data = cache.get('run_state')
            # if Path(self.config).is_file():
            #     logger.debug('loading state data')
            #     data = json.loads(open(self.config, 'rt').read())
            if 'tstamp' in data and data['tstamp'] >= 0:
                self.tstamp = data['tstamp']
                logger.info("loaded tstamp {}".format(data['tstamp']))
            if 'lifetime' in data and data['lifetime']:
                self.lifetime = data['lifetime']
                logger.info("loaded lifetime hit data {}".format(len(data['lifetime'].keys())))
            else:
                logger.info('No last timestamp found, defaulting to 0')

    @property
    def tstamp(self):
        return self.__tstamp

    @tstamp.setter
    def tstamp(self, tstamp):
        if tstamp < 0:
            raise ValueError('Must be a positive int')
        if tstamp < self.__tstamp:
            raise ValueError('Must be greater than last timestamp ' + self.__tstamp)
        logger.debug('tstamp now {}'.format(tstamp))
        self.__tstamp = tstamp

    def save_state(self):
        data = {
            'tstamp': self.tstamp,
            'lifetime': self.lifetime
        }
        # fh = open(self.config, 'wt')
        # fh = open(self.config.absolute(), 'wt')
        # fh.write(json.dumps(data))
        # fh.close()
        cache.set('run_state', data, expire=86400 * 20)

    # def __del__(self):
    #     if self.tstamp:
    #         logger.debug('logging {} before destroying parser'.format(str(self.tstamp)))
    #         self.save_state()


    def parse(self, **kwargs):
        wanted = ['SRC', 'DPT', 'SPT', 'DST', 'IN', 'OUT', 'MAC', 'UID']
        for line in fileinput.input():
            line = line.rstrip()
            parts = line.split()
            dt = datetime.datetime.strptime("{} {} {} {}".format(datetime.datetime.now().year, parts[0], parts[1], parts[2]), '%Y %b %d %H:%M:%S')
            utc_dt = dt + self.UTC_OFFSET_TIMEDELTA
            #tstamp = int(utc_dt.strftime("%s"))
            tstamp = int(dt.strftime("%s"))
            if self.tstamp >= tstamp:
                logger.info('skipping {} is older'.format(tstamp))
                continue
            else:
                self.tstamp = tstamp
                self.resumed = True
            data = dict(pair_re.findall(line))
            # "time": dt.strftime('%Y-%m-%dT%H:%M:%SZ'),
            # "time": int((dt - datetime.datetime(1970,1,1)).total_seconds()) + 60 * 60 + 8,
            # "datetime": dt.strftime('%Y-%m-%d %H:%M:%S'),
            # "utc_datetime": utc_dt.strftime('%Y-%m-%d %H:%M:%S'),
            payload = {
                "measurement": "blocks",
                "tags": {
                },
                "time": utc_dt.strftime('%Y-%m-%dT%H:%M:%SZ'),
                "fields": {
                    "hit": 1
                }
            }
            if parts[3]:
                payload['tags']['device'] = parts[3]
            if not ('DST' in data and 'SRC' in data):
                logger.warn('we are missing DST or SRC')
                breakpoint
                continue

            if ( data['DST'] in self.wan_ips or ipaddress.ip_address(data['DST']) in ip_network ):
                payload['tags']['direction'] = 'inbound'
                device_name = self.get_hostname_for_ip(data['DST'])
                payload['tags']['local_device'] = device_name
                key = data['SRC']
            elif ipaddress.ip_address(data['SRC']) in ip_network or data['SRC'] in self.wan_ips:
                device_name = self.get_hostname_for_ip(data['SRC'])
                payload['tags']['local_device'] = device_name
                payload['tags']['direction'] = 'outbound'
                key = data['DST']
            else:
                logger.info('Discovered WAN IP: {}'.format(data['DST']))
                device_name = self.get_hostname_for_ip(data['DST'])
                payload['tags']['local_device'] = device_name
                payload['tags']['direction'] = 'inbound'
                self.wan_ips.add(data['DST'])
                key = data['SRC']
            if key not in self.lifetime:
                self.lifetime[key] = 1
            self.lifetime[key] += 1
            payload['fields']['ioc'] = key
            # payload['fields']['ioc'] = key.replace('.', '_')
            payload['fields']['lifetime'] = self.lifetime[key]

                # # this may cause a bug if WAN IP changes mid stream
                # elif data['DST'] == self.wan_ip:
                #     payload['tags']['direction'] = 'inbound'
                # else:
                #     payload['tags']['direction'] = 'outbound'
                #     if 'DST' in data:
                #         key = data['DST']
                #         if key not in self.lifetime:
                #             self.lifetime[key] = 1
                #         self.lifetime[key] += 1
                #         payload['fields']['lifetime'] = self.lifetime[key]
                #         payload['fields']['destination'] = data['DST']
                #     if 'SRC' in data:
                #         payload['fields']['destination'] = data['SRC']



            for key in wanted:
                if key in data and data[key]:
                    payload['tags'][key] = data[key]
            self.blocks.append(payload)
            logger.debug('Added payload {}'.format(payload))
        
        # breakpoint()
        try:
            client.write_points(self.blocks)
            logger.info("Wrote {} items to influxdb".format(len(self.blocks)))
            logger.debug(self.blocks[0:3])
            try:
                logger.debug('saving state')
                self.save_state()
            except Exception as err:
                logger.error(err)
        except Exception as err:
            logger.error(err)


if __name__ == "__main__":
    lp = LogParser()
    lp.parse()
